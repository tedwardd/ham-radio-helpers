#!/bin/bash
# Command can be called wihth arguments or to read from stdin
# Author: Ted Wood K4KPW
# Original script by Robert Harder (K6RWH)'s Callbook.info lookup script

# CHANGELOG
## Use callbook JSON output instead of plaintext to enable parsing multiple fields
## Grab GRID from callbook.info in addition to name information
## Add bearing lookup from chris.org maidenhead bearing lookup tool
## Extract and print lat/lon information for target grid

# README
## This script provides information about a target callsign using multiple online resources.
## The only configuration necessary is to modify the value of "mygrid" under the "Constants"
## This should be set to your 6-character grid square. You can find your grid square here:
## http://www.levinecentral.com/ham/grid_square.php


####

# Constants
mygrid="em73ut" # Put your 6-character grid square here

# Echoes callsign and name of the operator
function _callsign {
    CS=$(echo $1 | tr "[:lower:]" "[:upper:]")
    data=$(curl -s "http://callook.info/${CS}/json")
    NAME=$(jq -r .name <<< $data)
    ADDR1=$(jq -r .address.line1 <<< $data)
    ADDR2=$(jq -r .address.line2 <<< $data)
    GRID=$(jq -r .location.gridsquare <<< $data)
}

function query {
	curl -s --data "mygrid=${mygrid}&fagrid=${1}" https://www.chris.org/cgi-bin/showdis
}

# Checks arguments
if [ $# -ge 1 ] # Callsigns passed in as arguments
then
    for cs in $@; do
        _callsign "$cs"
	grid_data=$(query "$GRID")
        echo $CS, $NAME, $GRID
	echo $ADDR1
	echo $ADDR2
	egrep 'bearing [0-9]+\.[0-9]+' <<< "$grid_data"
	latlon=$(awk -v grid=$(tr "[:lower:]" "[:upper:]" <<< "${GRID}:") '$0~grid{print $3"," $5}' <<< "$grid_data" | tr -d '=')
	echo "${GRID} is located at ${latlon}"
done
else            # Callsigns read from stdin
    xargs $0
fi
exit 0
