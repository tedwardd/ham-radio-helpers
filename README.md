These are some tools I've written for myself to aid in my amateur radio
endeavors. They are published publicly in the hopes that others may find them
useful as well. They are offered as-is and without warranty or guarantee. All
code published here is offered under the MIT license. You're welcome to modify
this code however you like and use it for your own purposes.  If you are
feeling so generous, please include credit back to me where applicable and if
you're feel especially generous, send me your improvements so I can improve my
own projects too.

All tools, when possible, have some documentation in the file headers. I
strongly recommend reading through the code for each tool before you use it as
some tools require that you modify variables or other parameters for your
specific situation.
